import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppConfigureService } from './service/app-configure.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
