import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConfigureService } from 'src/app/service/app-configure.service';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.scss'],
})
export class LoginpageComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  });
  isValidUser!: any;
  constructor(
    public appConfigureService: AppConfigureService,
    private router: Router
  ) {}

  submit(e: Event) {
    console.log(this.loginForm.value);

    this.isValidUser = this.appConfigureService.verifyUser(
      this.loginForm.value
    );
    console.log(this.isValidUser);
    if (this.isValidUser) {
      this.router.navigate(['home']);
    } else {
      alert('Invalid-user');
    }
  }

  ngOnInit(): void {}
}
