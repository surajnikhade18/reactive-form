import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppConfigureService {
  verifiedUser!: boolean;
  dummyValue = [
    {
      username: 'suraj',
      password: '1234',
    },
    {
      username: 'sahil',
      password: '1234',
    },
    {
      username: 'rahul',
      password: '1234',
    },
  ];

  constructor() {}

  verifyUser(obj: any) {
    if (
      this.dummyValue.find(
        (item) =>
          item.username === obj.username && item.password === obj.password
      )
    ) {
      this.verifiedUser = true;
    }
    return this.verifiedUser;
  }
}
